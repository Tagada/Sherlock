class Scoreboard

  def initialize channel, bot
    @channel = channel
    @bot = bot
    @score = File.exist?('duckhunt-' + @channel.to_s + '.yaml') ? (YAML.load(File.read('duckhunt-' + @channel.to_s + '.yaml')))[:score] : {}
    @score = @score.nil? ? {} : @score
  end

  def save
    @bot.synchronize(:scoreboard) do
      file = YAML::Store.new 'duckhunt-' + @channel.to_s + '.yaml'

      file.transaction do
        file[:score] = @score
      end
    end
  end

  def score user, method
    @bot.synchronize(:scoreboard) do
      @score[method.to_s] = {} if @score[method.to_s].nil?
      @score[method.to_s][user.to_s] = 0 if @score[method.to_s][user.to_s].nil?
      @score[method.to_s][user.to_s] += 1
    end
  end

  def top
    score = {}
    @bot.synchronize(:scoreboard) do
      score['bef'] = @score['bef'].sort {|a, b| b[1] <=> a[1]} unless @score['bef'].nil?
      score['bang'] = @score['bang'].sort {|a, b| b[1] <=> a[1]} unless @score['bang'].nil?
    end
    score
  end

  def topn n
    score = top
    score['bef'] = score['bef'].take(n) unless score['bef'].nil?
    score['bang'] = score['bang'].take(n) unless score['bang'].nil?
    score
  end

end


