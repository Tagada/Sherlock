class Config

  attr_reader :formats, :max_cooldown, :min_cooldown

  def initialize config
    @formats = [
        "・  ゜゜・。。・゜゜\\_0< QUACK!",
        "\\_O< ~ /kwɛ̃/ !",
        "BWAAAAAAAAAAAK! ・  ゜゜・。。・゜゜\\_0<"
    ]
    @max_cooldown = config[:max_cooldown]
    @min_cooldown = config[:min_cooldown]
  end
end