require_relative 'scoreboard'
require_relative 'config'

class Game
  include Cinch::Helpers

  attr_reader :last_duck, :next_duck, :duck

  def initialize channel, bot, config
    @config = Config.new config
    @scoreboard = Scoreboard.new channel, bot
    @channel = channel
    @bot = bot
    @started = true

    pool
  end

  def stop
    @timer.stop
    @stated = false
    save
  end

  def save
    @scoreboard.save
  end

  def bot
    @bot
  end

  def pool t = random
    return if @duck

    @last_pool = Time.now
    @next_duck = @last_pool + t
    @timer.stop unless @timer.nil?
    @timer = Timer(t, shots: 1) do
      format = @config.formats.sample

      format.dup.insert(Random.rand(format.length - 1), "\u{200B}")

      @channel.send format

      @last_duck = Time.now

      @duck = true
    end
  end

  # @return [Integer] random
  def random
    Random.rand(@config.max_cooldown - @config.min_cooldown) + @config.min_cooldown
  end

  def catch user, method
    if @duck
      catch_time = (Time.now - @last_duck).ceil(3).to_s

      case method
        when :bef
          @channel.send user.to_s + " has a new Ducky friend ! [" + catch_time + " s]"
          @scoreboard.score user, :bef
        when :bang
          @channel.send user.to_s + " killed a duck. [" + catch_time + " s]"
          @scoreboard.score user, :bang
        else
          @channel.send "Error"
      end

      @duck = false

      pool
    else
      case method
        when :bef
          @channel.send user.to_s + ", wtf dude? Hugging yourself?"
        when :bang
          @channel.send user.to_s + ", there's no duck to shot."
        else
          @channel.send "Error"
      end
    end
  end

  def send_score m, method, n = nil
    score = n.nil? ? @scoreboard.top : @scoreboard.topn(n)

    if score["bef"].nil?
      message_bef = "No !bef yet..."
    else
      message_bef = "Top !bef :"
      score["bef"].each {|user, points| message_bef += ' ' + user.dup.insert(1, "\u{200B}") + ' ' + points.to_s + ','}
    end

    if score["bang"].nil?
      message_bang = "No !bang yet..."
    else
      message_bang = "Top !bang :"
      score["bang"].each {|user, points| message_bang += ' ' + user.dup.insert(1, "\u{200B}") + ' ' + points.to_s + ','}.to_s
    end

    case method
      when :channel
        @channel.send message_bef[0...-1]
        @channel.send message_bang[0...-1]
      when :private
        m.user.safe_notice message_bef[0...-1]
        m.user.safe_notice message_bang[0...-1]
      else
        debug("Bad method")
    end
  end

end
