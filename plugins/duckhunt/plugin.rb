require_relative 'game'

=begin
Todo :
match /dh merge (#\w+) (.+) into (.+)/i, method: :admin_merge, react_on: :private

match /dh formats/i, method: :admin_formats_list, react_on: :private

match /dh formats add (.+)/i, method: :admin_formats_add, react_on: :private

match /dh formats rm (\d+)/i, method: :admin_formats_rm, react_on: :private
=end

module Plugins
  class Duckhunt
    include Cinch::Plugin
    include Cinch::Extensions::Authentication

    set :prefix, "!"

    def initialize *args
      super

      @games = {}
    end

    listen_to :join, method: :on_join
    def on_join m
      return unless m.user == bot

      @games[m.channel] = Game.new(m.channel, bot, config)
    end

    listen_to :leaving, method: :on_part
    def on_part m, user
      return unless user == bot

      @games[m.channel].stop
    end

    listen_to :disconnect, method: :on_disconnect
    def on_disconnect m
      @games.each {|game| game[1].stop}
    end

    match /bef/i, method: :bef, react_on: :channel
    def bef m
      @games[m.channel].catch m.user, :bef
    end

    match /bang/i, method: :bang, react_on: :channel
    def bang m
      @games[m.channel].catch m.user, :bang
    end

    match /ducks/i, method: :ducks, react_on: :channel
    def ducks m
      @games[m.channel].send_score m, :channel, 10
    end

    match /allducks/i, method: :allducks, react_on: :channel
    def allducks m
      @games[m.channel].send_score m, :private
    end

    # ADMIN

    match /dh timers/i, method: :admin_timers, react_on: :private
    def admin_timers m
      return unless authenticated? m

      @games.each {|game| m.reply "Next duck on " + game[0].to_s + ": " + game[1].next_duck.to_s}
    end

    match /dh save/i, method: :admin_save, react_on: :private
    def admin_save m
      return unless authenticated? m

      @games.each {|game| game[1].save}
      m.reply "Duckhunt saved."
    end

    match /dh pool (#\w+) (\d+)/i, method: :admin_pool, react_on: :private
    def admin_pool m, channel, time
      return unless authenticated? m
      return unless @games.has_key? Channel(channel)
      game = @games[Channel(channel)]
      return if game.duck

      game.pool time.to_i
      m.reply "Next duck on " + channel.to_s + ": " + game.next_duck.to_s
    end

  end
end
