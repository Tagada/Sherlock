module Plugins
  class Admin
    include Cinch::Plugin
    include Cinch::Extensions::Authentication

    enable_authentication
    set :prefix, "!"

    match /join (#\S+)/i, method: :join, react_on: :private
    def join m, channel
      Channel(channel).join
      m.reply channel + " joined."
    end

    match /leave (#\S+)/i, method: :leave, react_on: :private
    def leave m, channel
      Channel(channel).part "A+ dans le bus"
      m.reply channel + " leaved."
    end

    match /quit/i, method: :quit, react_on: :private
    def quit m
      @bot.quit
    end

  end
end
