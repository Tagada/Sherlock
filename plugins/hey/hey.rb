module Plugins
  class Hey
    include Cinch::Plugin
    include Cinch::Extensions::Authentication

    def initialize(*args)
      super

      @first = true
    end

    listen_to :join
    def listen m
      if @first && m.user == bot # Introduce myself
        m.reply 'Élémentaire, mon cher Watson.'
        m.reply 'Sherlock ' + config[:version].to_s + ' http://github.com/Sirgue/Sherlock'
        @first = false
      end
    end

    match /help/i, method: :help, react_on: :channel, prefix: "."
    def help m
      m.user.safe_notice 'Sherlock ' + config[:version].to_s + ' .help :'
      m.user.safe_notice 'DuckHunt : !bef, !bang, !ducks, !allducks'
      m.user.safe_notice 'Youtube : !youtube|!yt <search>'
      m.user.safe_notice 'Quotes : !kaamelott, !10map, !george, !quote'
      if authenticated? m
        m.user.safe_notice '## ADMIN ##'
        m.user.safe_notice 'Sherlock : !join #channel, !leave #channel, !quit'
        m.user.safe_notice 'Quotes : !quotes list, !quotes list <category>, !quotes add <category> <quote>, !quotes del <category> <id>'
        m.user.safe_notice 'Duckhunt : !dh save, !dh pool [t], !dh timers'
      end
    end

  end
end
