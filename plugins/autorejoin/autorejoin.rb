module Plugins
  class AutoRejoin
    include Cinch::Plugin

    listen_to :kick
    def listen m
      return unless User(m.params[1]) == bot
      Timer(5, shots: 1) { Channel(m.params[0]).join }
    end

  end
end
