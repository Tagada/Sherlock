require 'yaml/store'

module Plugins
  class Quotes
    include Cinch::Plugin
    include Cinch::Extensions::Authentication

    def initialize(*args)
      super

      @quotes = YAML.load(File.read('quotes.yaml'))['quotes']
    end

    def save_quotes
      file = YAML::Store.new "quotes.yaml"

      file.transaction do
        file['quotes'] = @quotes
      end
    end

    set :prefix, "!"

    match /10map ?(\d+)?$/i, method: :dixmap
    match /zboub ?(\d+)?$/i, method: :dixmap
    def dixmap m, id
      number = id.nil? ? Random.rand(@quotes[:dixmap].count) : id.to_i
      m.reply @quotes[:dixmap][number]
    end

    match /kaamelott ?(\d+)?$/i, method: :kaamelott
    match /akadok ?(\d+)?$/i, method: :kaamelott
    def kaamelott m, id
      number = id.nil? ? Random.rand(@quotes[:kaamelott].count) : id.to_i
      m.reply @quotes[:kaamelott][number]
    end

    match /laclasse ?(\d+)?$/i, method: :laclasse
    match /george ?(\d+)?$/i, method: :laclasse
    def laclasse m, id
      number = id.nil? ? Random.rand(@quotes[:laclasse].count) : id.to_i
      m.reply @quotes[:laclasse][number]
    end

    match /quote ?(\d+)?$/i, method: :quote
    def quote m, id
      number = id.nil? ? Random.rand(@quotes[:custom].count) : id.to_i
      m.reply @quotes[:custom][number]
    end

    # ADMIN

    match /quotes list ?(\w+)?$/i, method: :quotes_list, react_on: :private
    def quotes_list m, category
      return unless authenticated? m

      if category.nil?
        @quotes.each do |cat|
          m.user.safe_notice cat[0].to_s
        end
      else
        category = category.to_sym

        return if @quotes[category].nil?

        @quotes[category].each do |quote|
          m.user.safe_notice "ID: " + @quotes[category].index(quote).to_s + " | " + quote unless quote.nil?
        end
      end
    end

    match /quotes add (\w+) (.+)$/i, method: :quotes_add, react_on: :private
    def quotes_add m, category, quote
      return unless authenticated? m

      category = category.to_sym

      return if @quotes[category].nil?

      if @quotes[category].include? quote
        m.reply "Existe déjà. ID: " + @quotes[category].index(quote)
        return
      end

      @quotes[category] << quote

      m.user.safe_notice "Added quote ID: " + @quotes[category].index(quote).to_s + " | \"" + quote + "\""

      save_quotes
    end

    match /quotes rm (\w+) (\d+)$/i, method: :quotes_rm, react_on: :private
    def quotes_rm m, category, id
      return unless authenticated? m

      category = category.to_sym

      return if @quotes[category].nil?

      m.user.safe_notice "Deleted quote ID: " + id + " | \"" + @quotes[category][id.to_i] + "\""

      @quotes[category][id.to_i] = nil

      save_quotes
    end

  end
end
