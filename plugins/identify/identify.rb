module Plugins
  class Identify
    include Cinch::Plugin

    listen_to :connect, method: :identify
    def identify m
      mango = User('Mango')
      if mango
        mango.send "IDENTIFY " + config[:user] + " " + config[:password]
      end
    end

  end
end
