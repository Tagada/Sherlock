require 'metainspector'

module Plugins
  class Url
    include Cinch::Plugin

    URL_REGEX = /https?:\/\/\S+/i

    match URL_REGEX
    def execute m
      config[:exclude].each {|regex| return if Regexp.new(regex).match? m.message}

      urls = m.message.scan(URL_REGEX)
      urls.take(3).each do |url|
        begin
          title = (MetaInspector.new url, html_conten_only: true).title

          if title&.size > 0
            title = Cinch::Helpers.sanitize(title.gsub(/\s+/, ' ')[0...512].strip)

            m.reply title
          end
        rescue StandardError => e
          debug e.message
          debug e.backtrace.inspect
        end
      end
    end

  end
end
