module Plugins
  class CestPasFaux
    include Cinch::Plugin

    REGEXPS = [/C'est pas faux/i, /cépafo/i]

    def initialize(*args)
      super

      REGEXPS << /^(\s+)|([^a-zA-Z0-9]+)$/
    end

    listen_to :channel
    def listen m
      REGEXPS.each do |regexp|
        return if regexp.match? m.message
      end
      @previous = m.message
    end

    REGEXPS.each {|regexp| match regexp, react_on: :channel}
    def execute m
      return unless @previous

      words = @previous.split " "

      m.reply m.user.to_s + ", c'est \"" + words[words.length == 0 ? 0 : Random.rand(words.length)] + "\" que tu comprends pas ?"
    end

  end
end
