require 'yt'

module Plugins
  class Youtube
    include Cinch::Plugin

    def initialize(*args)
      super

      @last_id = nil
      @last_query = nil

      Yt.configuration.api_key = config[:api_key]
    end

    match /youtube/i, method: :search, react_on: :channel, prefix: "!"
    match /yt/i, method: :search, react_on: :channel, prefix: "!"
    def search m
      query = /\S+\s+(?<query>.*)/i.match(m.message)[:query]

      @last_query == query ? return : @last_query = query

      videos = Yt::Collections::Videos.new.where(q: query, safe_search: 'none', order: 'relevance').take(5)

      videos.each {|video| m.user.safe_notice video.title + " | https://www.youtube.com/watch?v=" + video.id}
    end

    match /(?:youtube(?:-nocookie)?\.com\/(?:[^\/\n\s]+\/\S+\/|(?:v|e(?:mbed)?)\/|\S*?[?&]v=)|youtu\.be\/)([a-zA-Z0-9_-]{11})/i, method: :url, react_on: :channel, prefix: nil
    def url m, id
      @last_id == id ? return : @last_id = id

      synchronize(:same_id) do
        begin
          video = Yt::Video.new id: id
          return if video.invalid?

          title = video.title
          like_count = video.like_count.to_s
          dislike_count = video.dislike_count.to_s
          view_count = video.view_count.to_s
          channel_title = video.channel_title
          published_at = video.published_at.strftime(Format("%d/%m/%Y"))
          
          if Time.at(video.duration) > Time.at(3600)
            length = Time.at(video.duration).utc.strftime("%T")
          else
            length = Time.at(video.duration).utc.strftime("%M:%S")
          end

          message = title + " [" + length + "] | "
          message += "+" + like_count + " -" + dislike_count + " | "
          message += view_count + " vues | "
          message += channel_title + ", le " + published_at

          m.reply message
        rescue StandardError
          m.user.safe_notice "Vidéo inexistante."
        end
      end
    end

  end
end
