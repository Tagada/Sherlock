# Sherlock
forked from [Oli4242](https://github.com/Oli4242)/Yuko

# Help
- Sherloc : .help
- DuckHunt : !bef, !bang, !ducks, !allducks
- Youtube : !youtube|!yt <search>
- Quotes : !kaamelott, !10map, !george, !quote

# Install
```bash
git clone https://github.com/Sirgue/Sherlock.git
cd Sherlock
bundle install
```

# Usage

```bash
mv config.example.yaml config.yaml
vim config.yaml
ruby Sherlock
```
